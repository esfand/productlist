﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piotr.ProductList.Model
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}